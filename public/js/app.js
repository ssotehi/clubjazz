/* Service web pour récuperer les evénements depuis la base de donnée.   */

/*

$( function() {

    console.log("JQuery ready!!!!!!.");
    var events = new Array(); // pour recupérer les evenements
    
    // configuration pour le calendrier
    var settings = {
        Color: 'gray',
        LinkColor: 'hsl(171, 25%, 36%)',
        NavShow: true,
        NavVertical: false,
        NavLocation: '',
        DateTimeShow: true,
        DateTimeFormat: 'mmm, yyyy',
        DatetimeLocation: '',
        EventClick: '',
        EventTargetWholeDay: false,
        DisabledDays: [],
    };

    // créer l'objet ajax XmlHttpRequest
    var jxhr = $.ajax({

        //url: '/caleandar',  
        url: "{{ path('caleandar_action') }}", // url du service caleandar
        type:       'GET',  // methode utlisée  
        async:      true,   // requêtte asyncrone
        datatype: 'json',
        success: function(){
        
            // la table recupérée comme objet json
            var eventArrays = JSON.parse(jxhr.responseText);
            
            for(idx = 0; idx < eventArrays.length; idx++)
            {   
                // objet temporaire pour stocker les données au format du plugin caleandar.min.js
                var obj = {}; 
                obj.Date  = new Date(   // créer la date de l'evenement
                                    eventArrays[idx].date.year, 
                                    eventArrays[idx].date.month-1,  // la date commence à zero
                                    eventArrays[idx].date.day
                                    );
                obj.Title = "<h3 >"+eventArrays[idx].title+
                ' à '+eventArrays[idx].beginAt+"h.</h3>";
                obj.Link = "/event/"+eventArrays[idx].id; 
                events.push(obj);  // stocker les evenements au format du calendrier                          
            }
 
            console.log("ajax success.");
            console.log(events);
        },
        error: function() { console.log("error occurred.") },
        complete: function() {
            
            // recupérer l'objet dom de avec son id
            var element = document.getElementById('caleandar');
            // passé l'elément, les evenements et la configuration à l'objet caleandar 
            caleandar(element, events, settings);

            console.log("Caleandar created.");
            console.log("ajax complete.") }
    }); 

return false;
});  */
