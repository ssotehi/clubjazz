<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HistoricController extends AbstractController
{
    /**
     * @Route("/historic", name="historic_action")
     * @return Response
     * @throws \Exception
     */
    public function historic() : Response
    {
        return $this->render('historic/index.html.twig', [
            //'controller_name' => 'HistoricController',
        ]);
    }

}
