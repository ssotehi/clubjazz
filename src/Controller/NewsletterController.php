<?php

namespace App\Controller;

use \Swift_Mailer;
use \Swift_Message;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class NewsletterController extends AbstractController
{
    /**
     * @Route("/newsletter", name="newsletter_action", methods="POST")
     * @param  Request $request
     * @param  \Swift_Mailer $mailer
     * @return Response
     * @throws \Exception
     */
    public function newsletter(Request $request,\Swift_Mailer $mailer) : Response
    {   
        if($request->isXmlHttpRequest()) {
   
            //$admin = new Admin();
            // récuperer l'adresse email de la requête Ajax
            $email = $request->request->get('email');

            // Create a message
            $message = (new \Swift_Message('Un nouvel article est publié'))
                ->setFrom('contact@soarcirta.com')
                ->setTo($email)
                ->setBody(
                    $this->renderView(
                        'email/email.news.html.twig', ['email' => $email]
                    ),
                    'text/html'
                )
            ;

            // Send the message
            $mailer->send($message);
            $this->addFlash('success', 'la newsletter est envoyé'); 

            $confirm = '<div class="alert alert-success alert-dismissible fade show" role="alert"> 
            Vous êtes inscrit(e) à la newsletter Club Convergences.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>';
            
            return new JsonResponse($confirm);

            }

        return $this->redirectToRoute('home_action');
    }
}
