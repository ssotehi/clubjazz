<?php
namespace App\Controller;

use App\Entity\Event;
use App\Repository\EventRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EventController extends AbstractController
{
    /**
     * @Route("/event", name="event_action")
     * @return Response
     * @throws \Exception
     */
    public function event() : Response
    { 
        $events = $this->getDoctrine()
                       ->getRepository(Event::class)
                       ->findAll();
        
        return $this->render('event/index.html.twig',
            ['events' => $events]
        );
    }

    /**
     * @Route("/event/{id}", name="show_action")
     * @return Response
     * @throws \Exception
     */
    public function show($id) : Response
    {    
        $event = $this->getDoctrine()
                       ->getRepository(Event::class)
                       ->find($id);

        return $this->render('event/show.html.twig', 
            ['event' =>  $event]
        );
    }
}
