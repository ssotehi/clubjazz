<?php

namespace App\Controller;

use App\Entity\Event;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CaleandarController extends AbstractController
{
    /**
     * @Route("/caleandar", name="caleandar_action", methods="GET")
     * @param  Request $request
     * @return Response
     * @throws \Exception
     */
    public function caleandar(Request $request) : Response
    {  
        if($request->isXmlHttpRequest()) {
            //$arrayCollection = $eventRepository->findAll();
            $arrayCollection = $this->getDoctrine()
                                    ->getRepository(Event::class)
                                    ->findAll();
            $events = array();

            // build json response
            foreach($arrayCollection as $item) {
                $events[] = array(
                    'id'    => $item->getId(),
                    'title' => $item->getTitle(),
                    'date'  => [
                        'year' => $item->getBeginAt()->format("Y"),
                        'month' => $item->getBeginAt()->format("m"),
                        'day' => $item->getBeginAt()->format("d")
                    ],
                    'beginAt' => $item->getBeginAt()->format("H:i:s"),
                    //'endTime' => $item->getEndAt()->format("H:i:s"),

                );
            }
        return new JsonResponse($events);
        }

        return new JsonResponse("Requêtte ajax non valide.");
    }
}
