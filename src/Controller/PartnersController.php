<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PartnersController extends AbstractController
{
    /**
     * @Route("/partners", name="partners_action")
     * @return Response
     * @throws \Exception
     */
    public function partners() : Response
    {     
        return $this->render('partners/index.html.twig');
    }
}
