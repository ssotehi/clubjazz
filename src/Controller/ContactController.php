<?php

namespace App\Controller;

use \Swift_Mailer;
use \Swift_Message;
use App\Entity\Event;
//use App\Entity\Admin;

use App\Entity\Contact;
use App\Form\FormContactType;
use Swift_TransportException;
use App\Repository\EventRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact_action")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     * @throws \Exception
     */
    public function contact(Request $request, \Swift_Mailer $mailer) : Response
    {
        //$contact = new Contact();
        //$contact ->setEmail('email');
                 
        $admin = 'contact@soarcirta.com';

        $form = $this->createForm(FormContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->getData();
            
            //  $em = $this->getDoctrine()->getManager();
            //  $em->persist($contact);
            //  $em->flush();

            // Create message
            $message = (new \Swift_Message())
                ->setSubject($contact['subject'])
                ->setFrom($admin)
                ->setTo($contact['email'])
                ->setBody(
                    $this->renderView(
                        'email/email.contact.html.twig', [
                            'firstname' => $contact['firstname'],
                            'lastname' => $contact['lastname'],
                            'content' => $contact['content'],
                            ]
                    ),
                    'text/html'
                )
            ;

            try {
                $mailer->send($message); 
            } 
            catch (Swift_TransportException $e) {
                echo $e->getMessage();
            }
            

            $this->addFlash('success', 'Votre message mail est envoyé');

            return $this->redirectToRoute('home_action');
  
        }
         
        $events = $this->getDoctrine()
                       ->getRepository(Event::class)
                       ->findAll();
      
        $index = count($events);

        return $this->render('contact/index.html.twig', [
            'form' => $form->createView(),
            'event' => $events[($index - 1)],
        ]);

    }

}
