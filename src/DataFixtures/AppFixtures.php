<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    
    public function load(ObjectManager $manager)
    {
         
        $admin = new Admin();
        $admin->setUsername('admin')
              ->setLastname('Fenech')
              ->setFirstname('lora')
              ->setEmail('contact@soarcirta.com')
              ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
              ->setPassword($this->passwordEncoder->encodePassword(
                   $admin,
                   'root'))
              ->setEnabled(true);
      $manager->persist($admin);
      $manager->flush();

    }
}
