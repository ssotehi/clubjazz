<?php

namespace App\Form;

//use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class FormContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class,[
                        'attr' => ['placeholder' => 'Votre prénom ...'],
                        'attr' => ['id' => 'content-area'],
            ])
            ->add('lastname', TextType::class,['attr' => ['placeholder' => 'Votre nom ...']])
            ->add('email', EmailType::class,['attr' => ['placeholder' => 'Votre email ...']])
            ->add('subject', TextType::class,['attr' => ['placeholder' => 'Votre sujet ...']])
            ->add('content', TextareaType::class,['attr' => ['placeholder' => 'Votre message ...']])
            /*
            ->add('captcha', Recaptcha3Type::class, [
                'constraints' => new Recaptcha3(),
                'action' => 'contact_cation',
            ])*/
            //->add('send', SubmitType::class)
            ->getForm()
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        /*
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
        */
    }
}
